<?php

namespace Curator;

use Curator\Utilities\JewishDate;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class JDateTest extends Command
{
    private $play_date = '2020-05-26';

    protected function configure()
    {
        $this->setName('j-date-test')
            ->setDescription('Test converting Jewish/Hebrew dates back to gregorian dates.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $converter = new JewishDate();

        $hebrew_date = $converter->convertToHebrew($this->play_date);

        for ($i = $hebrew_date[2]; $i < $hebrew_date[2] + 20; $i++) {
            $g_date = $converter->convertFromHebrew($hebrew_date[0], $hebrew_date[1], $i);

            $output->writeln($g_date);
        }
    }
}