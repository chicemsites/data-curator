<?php

namespace Curator;

use Curator\DB;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class CSVIngester extends Command
{
    private $file_ext = '.csv';
    private $database = '';
    private $path = '';
    private $recursive = false;
    private $add_file_name_column = false;

    protected function configure()
    {
        $this->setName('ingest-csvs')
            ->setDescription('Parse CSVs and create database tables, inserting CSV data as we go.')
            ->addOption('recursive', '-r', InputOption::VALUE_NONE, 'Recursively scan directories for CSVs.')
            ->addOption('filename', '-f', InputOption::VALUE_NONE, 'Add an extra column to the data set and puts the file name in that column for every inserted row.')
            ->addArgument('database', InputArgument::REQUIRED, 'Insert the CSVs into this database. The database must be setup in your .env file.')
            ->addArgument('path', InputArgument::REQUIRED, 'The full path to the CSVs');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->database = strtoupper($input->getArgument('database'));
        $this->path = $input->getArgument('path');
        $this->recursive = $input->getOption('recursive');
        $this->add_file_name_column = $input->getOption('filename');

        $output->writeln('Attempting to scan for CSVs...');

        $file_cnt = $this->scanDirectoryForCSV($output, $this->path);

        $output->writeln('Processed ' . $file_cnt . ' CSVs');
    }

    private function scanDirectoryForCSV(OutputInterface $output, $path) {
        if (strrpos($path, '/') <> (strlen($path) - 1)) {
            $path .= '/';
        }

        $files = scandir($path);
        $file_cnt = 0;

        foreach ($files as $file) {
            if (in_array($file, ['.', '..'])) {
                continue;
            }

            $path_info = pathinfo($file);

            if (isset($path_info['extension'])) {
                switch($path_info['extension'])
                {
                    case 'csv':
                        $output->writeln('Processing ' . $file);

                        $table_name = $this->getTableName($file);

                        $output->writeln('Inserting into table ' . $table_name);

                        $this->ingestCSV($path, $file, $table_name, $output);

                        $file_cnt++;
                    break;

                    default:
                    break;
                }
            } elseif ($this->recursive
            && is_dir($path)
            ) {
                $this->scanDirectoryForCSV($output, $path . $file);
            }
        }

        return $file_cnt;
    }

    private function getTableName($file)
    {
        $table_name = str_replace($this->file_ext, '', $file);
        $table_name = preg_replace("/[^A-Za-z0-9 ]/", ' ', $table_name);

        while (strpos($table_name, '  ') !== false) {
            $table_name = str_replace('  ', ' ', $table_name);
        }

        $table_name = trim($table_name);
        $table_name = str_replace(' ', '_', $table_name);
        $table_name = strtolower($table_name);

        return $table_name;
    }

    private function getColumnNames($columns)
    {
        $column_names = [];

        foreach ($columns as $column) {
            if (empty($column)) {
                $column = 'unkown';
            }

            $column = preg_replace("/[^A-Za-z0-9 ]/", ' ', $column);

            while (strpos($column, '  ') !== false) {
                $column = str_replace('  ', ' ', $column);
            }

            $column = trim($column);
            $column = str_replace(' ', '_', $column);
            $column = strtolower($column);

            $column_names[] = $column;
        }

        $column_counts = array_count_values($column_names);

        $column_counts = array_diff($column_counts, [1]);

        foreach (array_reverse($column_names, true) as $key => $column) {
            if (isset($column_counts[$column])) {
                $number = $column_counts[$column]--;
                $column_names[$key] = $column .= "_{$number}";
            }
        }

        return $column_names;
    }

    private function ingestCSV($path, $file, $table_name, OutputInterface $output)
    {
        $column_names = [];
        $first_data_row = [];
        $table_created = false;
        $line = 0;

        $cnt_output = $output->section();

        try {
            if (($handle = fopen($path . $file, 'r')) !== false) {
                /* while (($buffer = fgetcsv($handle)) !== false) {
                    $line++;
                    $buffer = preg_replace('/(\\\",)/','\\ ",',$buffer);
                    $buffer = preg_replace('/(\\\"("?),)/',' ',$buffer);
                    $data = str_getcsv($buffer); */
                while (($data = fgetcsv($handle, 0, ',', '"', "")) !== false) {
                    if (empty($column_names)) {
                        $column_names = $this->getColumnNames($data);

                        if ($this->add_file_name_column) {
                            $column_names[] = 'data_source_filename';
                        }

                        continue;
                    }

                    if ($this->add_file_name_column) {
                        $data[] = $file;
                    }

                    if (!$this->isDataRecordEmpty($data)) {
                        if (empty($first_data_row)) {
                            foreach ($data as $col_value) {
                                $first_data_row[] = $col_value;
                            }
                        }
                    }

                    if (!empty($column_names)
                    && !empty($first_data_row)
                    && !$table_created
                    ) {
                        DB::warehouse($this->database)
                        ->table($table_name)
                        ->detectDataTypes($first_data_row)
                        ->create($column_names);

                        $table_created = true;

                        $insert_record = array_combine($column_names, $first_data_row);

                        DB::warehouse($this->database)
                        ->table($table_name)
                        ->insertOnNewTable($insert_record);
                    } else {
                        $insert_record = array_combine($column_names, $data);

                        if (!$this->isDataRecordEmpty($insert_record)) {
                            DB::warehouse($this->database)
                            ->table($table_name)
                            ->insertOnNewTable($insert_record);
                        }
                    }

                    $line++;
                    $cnt_output->clear();
                    $cnt_output->write('Inserted Record Count: ' . $line);
                }
            }
        } catch (Throwable $th) {
            $output->writeln('<error>' . $th->getMessage() . '</error>');
        }

        $cnt_output->clear();
        unset($cnt_output);
        $output->writeln('Final Inserted Record Count: ' . $line);
        $output->writeln('');
    }

    private function isDataRecordEmpty($record)
    {
        $empty = true;

        foreach ($record as $column) {
            if (trim($column) != '') {
                $empty = false;
                break;
            }
        }

        return $empty;
    }
}