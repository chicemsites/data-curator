<?php

namespace Curator;

use Curator\DB;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use DateTime;
use DateTimeZone;
use Symfony\Component\Console\Input\InputArgument;

class FileMigration extends Command
{
    private $database = '';
    private $config_folder = '';
    private $config_path = '';

    private $config_opt_blog_id = '';
    private $config_opt_prefix = '';
    private $config_opt_match_query = '';
    private $config_opt_source_path = '';
    private $config_opt_target_path = '';

    private $ids_list = [];

    protected function configure()
    {
        $this->setName('files:migrate')
            ->setDescription('Prepare files from their old system for upload to their new system.')
            ->addArgument('database', InputArgument::REQUIRED, 'The database to run against. Use the name from your ENV file.')
            ->addArgument('config', InputArgument::REQUIRED, 'The folder that the configuration file is in.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->database = strtoupper($input->getArgument('database'));
        $this->config_folder = $input->getArgument('config');

        $this->config_path = __DIR__ . '/Migrations/' . $this->config_folder . '/files.json';

        try {
            $config = file_get_contents($this->config_path);
            $config = json_decode($config);

            $this->config_opt_blog_id = $config->blog_id;
            $this->config_opt_prefix = $config->table_prefix;

            foreach ($config->tables as $upload_to) {
                $this->config_opt_source_path = $upload_to->source_path;
                $this->config_opt_target_path = $upload_to->target_path;
                $this->config_opt_match_query = $upload_to->match_query;

                $this->ids_list = $this->getIdList();

                $this->catalogFiles($this->config_opt_source_path);
            }

        } catch (\Throwable $th) {
            $output->writeln('<error>' . $th->getMessage() . '</error>');
        }
    }

    private function catalogFiles($path)
    {
        $files = scandir($path);
        foreach ($files as $file) {
            if (in_array($file, ['.', '..'])) {
                continue;
            }

            if (is_dir($path . '/' . $file)) {
                $this->catalogFiles($path . '/' . $file);
            } else {
                $match_with = $this->getIdFromFileName($file);

                if (isset($this->ids_list[$match_with])) {
                    $ids = $this->ids_list[$match_with];

                    DB::warehouse($this->database)
                        ->table($this->config_opt_prefix . 'uploads')
                        ->insert([
                            'account_id' => $ids['account_id'] == null ? '0' : $ids['account_id'],
                            'record_id' => $ids['record_id'],
                            'post_type' => 'grave',
                            'file' => $file,
                            'path' => '/home/cemsites3/file_secure/' . $this->config_opt_blog_id . '/' . $ids['record_id'],
                            'created_at' => (new DateTime('now', new DateTimeZone('America/New_York')))->format('Y-m-d')
                        ]);

                    $full_target_path = $this->config_opt_target_path . '/' . $ids['record_id'];

                    if (!file_exists($full_target_path)) {
                        mkdir($full_target_path);
                    }

                    rename(
                        $path . '/' . $file,
                        $full_target_path . '/' . $file
                    );
                }
            }
        }
    }

    private function getIdFromFileName($file_name)
    {
        $ext_pos = strrpos($file_name, '.');
        if ($ext_pos !== false) {
            $id = substr($file_name, 0, $ext_pos);
        }

        return $id;
        //return preg_replace("/[^0-9]/", "", $id);
    }

    private function getIdList()
    {
        $sql = $this->config_opt_match_query;

        $ids_list = DB::warehouse($this->database)->raw($sql)->get();

        foreach ($ids_list as $set) {
            $file_key = preg_replace("/[\"]/", "", $set['match_with']);
            $ret[$file_key] = $set;
        }

        return $ret;
    }
}