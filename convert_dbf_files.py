import dbf, subprocess, sys
from os import listdir, walk
from os.path import isfile, join

path_to_dbfs = '/home/naknak987/Documents/CemSites/Migrations/United Hebrew - 471/FoxProData/DBF/'
output_path = '/home/naknak987/Documents/CemSites/Migrations/United Hebrew - 471/FoxProData/CSV/'
output_path_for_command = '/home/naknak987/Documents/CemSites/Migrations/United Hebrew\ -\ 471/FoxProData/CSV/'

for (dirpath, dirnames, filenames) in walk(path_to_dbfs):
    f_names = filenames
    break;

couldnt_read = [
    'SOCIETY.DBF',
    'ACCT2.DBF',
    'ACCT.DBF',
    'GRAVE.DBF',
    'CUST2.DBF',
    'FOXVIN.DBF'
]

for f_name in f_names:
    try:
        if f_name in couldnt_read:
            full_command = 'dbf2csv \'' + path_to_dbfs + f_name + '\' \'' + output_path + f_name.replace('.dbf', '.csv').replace('.DBF', '.csv') + '\' -ie cp1252 -q all'

            print(full_command + ';')

            """ run_command = subprocess.run(full_command, capture_output=True)

            sys.stdout.buffer.write(run_command.stdout)
            sys.stdout.buffer.write(run_command.stderr)
            sys.exit(run_command.returncode) """
        """ else:
            csv_fn = output_path + f_name.replace('.dbf', '.csv')

            table = dbf.Table(path_to_dbfs + f_name)
            table.open()

            dbf.export(table, csv_fn, header = True)

            table.close() """

    except:
        print('Error: ' + f_name)

